# vim: set fileencoding=utf8 :

import Tkinter as tk
from Tkinter import *
import ttk
from ttk import *
import threading
import os
import time
import tkFont
import audio

pressed = False

def stop(): # Stops the robot
    running = open("running.lock", "w")
    running.write("False")
    running.close()

def start(): # Starts the robot
    running = open("running.lock", "w")
    running.write("True")
    running.close()

def runThread(): # Runs new thread and disables the button
    global pressed
    pressed = True
    kt = threading.Thread(target=runLoop)
    kt.start()

def runLoop(): # Runs program with values from checkboxes
    colours = ["b","y"]
    id_list = ["A","B","C","D"]

    # Different launch values
    opponent = colours[opponent_var.get()]
    field = id_list[field_var.get()]
    robotID = id_list[id_var.get()]

    print([opponent,field,robotID])
    os.system("sudo python /home/teamx/robotex2015/loop.py -v "+str(opponent)+" -f "+str(field)+" -i "+str(robotID))

def terminate(): # Kills all pythons
    os.system("sudo killall python")

def victory():
    running = open("running.lock", "w")
    running.write("False")
    running.close()
    audio.play("winsong.ogg")
    """
    for i in range(5):
        mootoridtest.testrotation(75)
        time.sleep(1)
        """

def changeButtonColour(): # Changes the button coulor based on opponents coulor
    global frame,pressed
    run_button = tk.Button(frame, text="Run", command = lambda :runThread(), bg = getColour())
    run_button.place(x=310, y=40,height=100, width=100)
    run_button['font'] = button_font
    while True:
        if pressed:
            run_button.configure(state=DISABLED)
            start_button.configure(state=NORMAL)
            stop_button.configure(state=NORMAL)
            win_button.configure(state=NORMAL)
            return
        colour = getColour()
        run_button.configure(bg = colour)
        time.sleep(0.2)

def getColour(): # Gets colour based on checkbox
    if opponent_var.get() == 0:
        return "blue"
    elif opponent_var.get() == 1:
        return "yellow"

# GUI

frame = tk.Tk()
frame.title("Robotex2015 - TeamX - Polli")
frame.resizable(False, False)
frame.geometry("420x195")

opponent_label = tk.Label(frame, text="Opponent:")
opponent_label.place(x=10, y=10)
opponent_var = IntVar()
opponent_var.set(0)
Radiobutton(frame, text="Blue", variable=opponent_var, value=0).place(x=10, y=40)
Radiobutton(frame, text="Yellow", variable=opponent_var, value=1).place(x=10, y=70)


field_label = ttk.Label(frame, text="Field:")
field_label.place(x=110, y=10)
field_var = IntVar()
field_var.set(0)
Radiobutton(frame, text="A", variable=field_var, value=0).place(x=110, y=40)
Radiobutton(frame, text="B", variable=field_var, value=1).place(x=110, y=70)


id_label = tk.Label(frame, text="ID:")
id_label.place(x=210, y=10)
id_var = IntVar()
id_var.set(0)
Radiobutton(frame, text="A", variable=id_var, value=0).place(x=210, y=40)
Radiobutton(frame, text="B", variable=id_var, value=1).place(x=210, y=70)
Radiobutton(frame, text="C", variable=id_var, value=2).place(x=210, y=100)
Radiobutton(frame, text="D", variable=id_var, value=3).place(x=210, y=130)

button_font = tkFont.Font(size = 14, weight = 'bold')

start_button = tk.Button(frame, text="Start", command = lambda :start(), bg = "green", state = DISABLED)
start_button.place(x=10, y=160, height=25, width=100)
start_button['font'] = button_font

stop_button = tk.Button(frame, text="Stop", command = lambda :stop(), bg ="red", state = DISABLED)
stop_button.place(x=130, y=160, height=25, width=100)
stop_button['font'] = button_font

terminate_button = tk.Button(frame, text="Terminate", command = lambda :terminate())
terminate_button.place(x=250, y=160, height=25, width=100)
terminate_button['font'] = button_font

win_button = tk.Button(frame, text="Win", command = lambda :victory(), state = DISABLED)
win_button.place(x=370, y=160, height=25, width=40)
win_button['font'] = button_font

bt = threading.Thread(target=changeButtonColour)
bt.start()

frame.mainloop()
