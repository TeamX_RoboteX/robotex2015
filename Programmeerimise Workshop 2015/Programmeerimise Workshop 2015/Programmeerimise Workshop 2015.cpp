// Programmeerimise Workshop 2015.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "boost/asio.hpp"

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>

#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

void firstTasks();
void hsvThreshold();
void serialExample();
void opencvTest();
void writeString(string data, boost::asio::serial_port serial);
string readLine(boost::asio::serial_port serial);

int _tmain(int argc, _TCHAR* argv[])
{
	//cin.get();
	
	//firstTasks()
	//hsvThreshold();
	serialExample();
	//opencvTest();
	return 0;
}

void firstTasks()
{
	VideoCapture cap(0);

	if (!cap.isOpened())
	{
		cout << "Can't open video device\n";
		cin.get();
		return;
	}

	namedWindow("Video 1");
	namedWindow("Video 2");

	int binThresh = 127;

	createTrackbar("Binary threshold", "Video 2", &binThresh, 255, nullptr);

	Mat frame;

	while (true)
	{
		cap >> frame;
		Mat hflip;
		Mat vflip;
		Mat gray;

		flip(frame, hflip, 0);
		flip(frame, vflip, 1);
		imshow("Video 1", hflip);
		imshow("Video 2", vflip);

		Vec3b pixel = hflip.at<Vec3b>(Point(100, 50));
		cout << (int)pixel[0] << ", " << (int)pixel[1] << ", " << (int)pixel[2] << endl;

		cvtColor(frame, gray, CV_BGR2GRAY);
		threshold(gray, gray, binThresh, 255, THRESH_BINARY);
		imshow("Video 2", gray);

		if (waitKey(10) == 27) break;

	}	
}

void hsvThreshold()
{
	VideoCapture cap(0);

	if (!cap.isOpened())
	{
		cout << "Can't open video device\n";
		cin.get();
		return;
	}

	namedWindow("Video 1");
	namedWindow("pre");
	namedWindow("er");
	namedWindow("di");
	

	unsigned char val[6];
	ifstream inf;
	inf.open("calib.dat", ios::binary | ios::in);
	inf.read(reinterpret_cast<char *>(val), sizeof(val));
	
	int hmin, smin, vmin, hmax, smax, vmax;

	//hmin = static_cast<unsigned char>(val[0]);
	//hmax = static_cast<unsigned char>(val[1]);
	hmin = val[0];
	hmax = val[1];
	smin = val[2];
	smax = val[3];
	vmin = val[4];
	vmax = val[5];
	
	createTrackbar("H min", "Video 1", &hmin, 255, nullptr);
	createTrackbar("S min", "Video 1", &smin, 255, nullptr);
	createTrackbar("V min", "Video 1", &vmin, 255, nullptr);

	createTrackbar("H max", "Video 1", &hmax, 255, nullptr);
	createTrackbar("S max", "Video 1", &smax, 255, nullptr);
	createTrackbar("V max", "Video 1", &vmax, 255, nullptr);

	Mat frame;
	Mat thresh;

	RNG rng(12345);

	while (true)
	{
		cap >> frame;
		cvtColor(frame, frame, COLOR_BGR2HSV);
		inRange(frame, Scalar(hmin, smin, vmin), Scalar(hmax, smax, vmax), thresh);

		imshow("Video 1", thresh);

		Mat canny_output = thresh.clone();
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		/// Detect edges using canny
		//Canny(thresh, canny_output, 100, 200, 3);

		//imshow("pre", canny_output);
		Mat element = getStructuringElement(MORPH_RECT, Size(5, 5), Point(2, 2));
		dilate(canny_output, canny_output, element);
		imshow("di", canny_output);
		element = getStructuringElement(MORPH_RECT, Size(7, 7), Point(3, 3));
		erode(canny_output, canny_output, element);
		imshow("er", canny_output);
		/// Find contours
		findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		

		/// Draw contours
		Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
		cout << contours.size() << endl;
		for (int i = 0; i< contours.size(); i++)
		{
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
		}

		/// Show in a window
		namedWindow("Contours", CV_WINDOW_AUTOSIZE);
		imshow("Contours", drawing);

		if (waitKey(10) == 27) break;
	}

	val[0] = getTrackbarPos("H min", "Video 1");
	val[1] = getTrackbarPos("H max", "Video 1");
	val[2] = getTrackbarPos("S min", "Video 1");
	val[3] = getTrackbarPos("S max", "Video 1");
	val[4] = getTrackbarPos("V min", "Video 1");
	val[5] = getTrackbarPos("V max", "Video 1");

	ofstream of;
	of.open("calib.dat", ios::binary | ios::out);

	of.write(reinterpret_cast<char*>(val), sizeof(val));
	of.close();
}

void serialExample()
{
	boost::asio::io_service io;
	boost::asio::serial_port serial(io, "COM7");
	serial.set_option(boost::asio::serial_port_base::baud_rate(115200));

	//string result;

	for (int i = 0; i < 1000; i++)
	{
		char c;
		string result;
		while (true)
		{
			boost::asio::read(serial, boost::asio::buffer(&c, 1));
			switch (c)
			{
			case '\n':
				break;
			default:
				result += c;
			}
		}
		cout << result << endl;
		this_thread::sleep_for(chrono::milliseconds(500));
	}



}

void writeString(string data, boost::asio::serial_port serial)
{
	boost::asio::write(serial, boost::asio::buffer(data.c_str(), data.size()));
}

string readLine(boost::asio::serial_port serial)
{
	char c;
	string result;
	while (true)
	{
		boost::asio::read(serial, boost::asio::buffer(&c, 1));
		switch (c)
		{
		case '\n':
			return result;
		default:
			result += c;
		}
	}
}

void opencvTest()
{
	VideoCapture cap(0);

	if (!cap.isOpened())
	{
		cout << "Can't open video device\n";
		cin.get();
		return;
	}

	namedWindow("Video 1");
	cin.get();


}