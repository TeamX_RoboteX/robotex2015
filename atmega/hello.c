#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

void usart_write(unsigned char* data) {
  //PORTB |= 0b00001100;
  //PORTD |= 0b00010000;
  while(*data) {
    while(!( UCSR1A & (1<<UDRE1)));
    PORTF |= 0b00000001;
    UDR1 = *data;
    data++;
  }
  _delay_ms(1000);
  PORTF &= 0b11111110;
  
  //PORTB &= 0b11110011;
  //PORTD &= 0b11101111;
}

int main () {
  /* Disables clock divider */
  CLKPR = 0x80;
  CLKPR = 0x00;
  /* Disables debugger for port f */
  MCUCR = _BV(JTD);
  MCUCR = _BV(JTD);
  /* data direction out */
  DDRF |= 0b00000001;
  /* set baudrate 19200 */
  UBRR1 = 51;
  /* Enable transmitter */
  //UCSR1B = 1<<TXEN1;
  /* Set frame format: 8data, 1stop(default) bit */
  //UCSR1C = (1<<UCSZ10)|(1<<UCSZ11);
  bit_set(DDRD, BIT(RS485TX) | BIT(RS485TXEN) | BIT(RS485RXEN));
  bit_clear(DDRD, BIT(RS485RX));
  
  bit_set(PORTD, BIT(RS485TXEN)); // enable Tx
  bit_clear(PORTD, BIT(RS485RXEN)); // enable Rx (inverted)
  UCSR1B = BIT(RXEN1) | BIT(TXEN1) | BIT(RXCIE1); // enable Tx, Rx
  UCSR1C = BIT(USBS1) | BITS(0b11, UCSZ10); // 2-bit stop, 8-bit character
  unsigned char tekst[] = {'a', 'A', '\0'};
  
  while (1) {
    usart_write(tekst);
    _delay_ms(1000);
  }
}
