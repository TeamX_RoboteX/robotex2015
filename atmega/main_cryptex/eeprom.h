#ifndef EEPROM_H
#define EEPROM_H

/** EEPROM
0: id
1: interrupt replies
2: kicktime
**/

#include <avr/eeprom.h>

#define EEPROM_ID ((uint8_t*)0)
#define EEPROM_INTRPL ((uint8_t*)1)
#define EEPROM_KICKTIME ((uint8_t*)2)

#endif
