import threading
import pygame

def shootAudio():
    pygame.mixer.init()
    pygame.mixer.music.load("moo.mp3")
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy() == True:
        continue

def shootMusic():
    try:
        p = threading.Thread(target=shootAudio)
        p.start()
    except:
        pass

def play(song): # Writes song to audio.lock
    running = open("audio.lock", "w")
    running.write(song)
    running.close()

def playSong(): # Reads audio.lock and plays it if it is not False
    lastSong = 'False'
    pygame.mixer.init()
    while True:
        running = open("audio.lock", "r")
        song = running.read()
        running.close()
        
        if len(song)>0:
            while lastSong == "winsong.ogg" and pygame.mixer.music.get_busy() == True:
                continue
            if lastSong == song and pygame.mixer.music.get_busy() == True:
                continue
            elif song == 'False' and pygame.mixer.music.get_busy() == True:
                #pygame.mixer.music.fadeout(200)
                lastSong=song
            elif lastSong != song:
                #pygame.mixer.music.fadeout(200)
                pygame.mixer.music.load(song)
                pygame.mixer.music.set_volume(0.7)
                pygame.mixer.music.play()
                lastSong=song
            elif pygame.mixer.music.get_busy() == False and song != 'False':
                pygame.mixer.music.load(song)
                pygame.mixer.music.set_volume(0.7)
                pygame.mixer.music.play()

