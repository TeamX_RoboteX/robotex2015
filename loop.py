#!/usr/bin/python
# -*- coding: utf-8 -*-

from lib import vision
from multiprocessing import Process
import threading
from time import sleep
import ctypes
import radio
from mootorid import *
from robotClass import *
from obj import obj
import audio
import sys
import logging
import datetime

logging.basicConfig(filename='zlogfile.txt'+str(datetime.datetime.now()), level=logging.DEBUG, format='%(asctime)s - %(message)s')

#this on e works in test.py :
# logging.basicConfig(filename='zlogs.txt'+str(datetime.datetime.now()), level=logging.DEBUG, format='%(asctime)s - %(message)s')


# logging.basicConfig(filename='log.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
# pohhui, fuck this logging bullshit, nothing seems to work on nuc anymore, 
# log file creation code which works perfectly on any other platform fails on nuc
logging.debug("kaoeidfjiaseufjufhsri")

### Configuration ###

noFallbacks = False # for testing only, True means no going back in decision tree(maximal signal tolerance).

### /Configuration ###

### Konstandid ###

RIGHT = 1
LEFT = -1

BLUE = 1
YELLOW = 2

OPPONENTGOALCOLOR = BLUE

look = 0
turn = 1
move = 2
aim = 3
shoot = 4

### /Konstandid ###

def loopStep():
    global executionState, mt, vision, robot, options, signaltolerance
    # Read radio input from file
    running = open("running.lock", "r")
    runningBool = eval(running.read())
    running.close()

    #try:
       # print(robot.surroundings.yellowGoal.y)
    #except:
       # print(False)

    if not runningBool:
        mt.setDribblerSpeed(0)
        mt.setMoveForward(0)
        mt.writeToMotors()
        time.sleep(0.1)
        return
    
    #    if executionState == 3: # Aim at goal
    mt.setDribblerSpeed(255)
#    else:
 #       mt.setDribblerSpeed(105)
    mt.charge()
    mt.ping()

    # What does it do?
    robot.surroundings.update()

    audio.play("False") # Plays gametime song
    
    signalState = robot.getState()

    # (["look for ball", "turn until ball", "move to ball", "aim at goal", "shoot"][executionState])
    # ("actual: " + ["look for ball", "turn until ball", "move to ball", "aim at goal", "shoot"][signalState])


    if executionState > signalState:  # signalState being smaller means going backwards in decision tree.
        robot.wrongSignalCount += 1
        if robot.wrongSignalCount > signaltolerance[executionState]:
            executionState = signalState
            #print(str(datetime.now())+"state: "+str(executionState)+": "+["look for ball", "turn until ball", "move to ball", "aim at goal", "shoot"][executionState])
            robot.wrongSignalCount = 0
    else: # executionState <= signalState:  # executionState greater than current means progress in decision tree.
        if executionState > signalState:
            pass
           # print(str(datetime.now())+"state: "+str(executionState)+": "+["look for ball", "turn until ball", "move to ball", "aim at goal", "shoot"][executionState])

        robot.wrongSignalCount = 0
        executionState = signalState

    # Execute behavior based on state.
    options[executionState]()

    # Send signals to motors.
    robot.writeToMotors()


def init():
    global executionState, mt, vision, robot, options, signaltolerance
    vision.init()
    
    running = open("running.lock", "w")
    running.write("False")
    running.close()

    running = open("audio.lock", "w")
    running.write("False")
    running.close()

    mt = Mootorid()

    ### Read commmand line arguments ###
     # -f : väljaku id A / B
     # -i : roboti id A / B / C / D
     # -v : vastase väljaku värv b / y
    
    if '-f' in sys.argv and '-i' in sys.argv:
        field = sys.argv[sys.argv.index('-f')+1]
        id = sys.argv[sys.argv.index('-i')+1]

        t = threading.Thread(target=radio.listen, args=(field, id))
        t.start()

    t = threading.Thread(target=radio.read)
    t.start()

    playSongThread = threading.Thread(target=audio.playSong)
    playSongThread.start()

    audio.play("morning.mp3")

    # Initiate robot class here
    if not '-v' in sys.argv:
        robot = Robot(vision, mt, BLUE)
    else:
        robot = Robot(vision, mt, {'b':BLUE, 'y':YELLOW}[sys.argv[sys.argv.index('-v')+1]])

    ### /Read command line arguments ###
        

    # mapping states to appropriate functions
    options = {
        0: robot.lookForBall,  # Ball not on screen, turn until see a ball
        1: robot.turnUntilBall,  # Turn until ball in center of vision
        2: robot.moveToBall,  # Move until ball safely in dribbler
        3: robot.aimAtGoal,  # Turn with ball until Goal in centre of vision
        4: robot.shoot,  # Shoot ball towards the goal
    }

    executionState = 0
    signaltolerance = {
        look: 0,
        turn: 0,
        move: 0,
        aim: 10,
        shoot: 0 # for some reason fires coilgun repeatedly if > 0 
    }

    # Main loop
    while True:
        loopStep()

if __name__ == '__main__':
    init()
