# -*- coding: windows-1250 -*-
# Goal:
# Robot can communicate with motor controllers via self written code.

# 120 puhul liikusid 1 ja 3 samas suunas

import serial
import time
import re
from math import *
from operator import add
import math
import logging

logging.basicConfig(filename='log.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


sign = lambda x: math.copysign(1, x)

PORT = '/dev/ttyUSB0'
BAUDRATE = 19200
LEFTWHEELID = 1
RIGHTWHEELID = 2
BACKWHEELID = 3


class Mootorid:
    def __init__(self):
        BAUDRATE = 19200
        PORT = '/dev/ttyUSB0'
        self.MAINBOARDID = '255'
        self.SER = serial.Serial(PORT, BAUDRATE, stopbits=serial.STOPBITS_TWO)
        #        self.polaarsustest()
        self.lastKicked = time.time()
        self.LEFTWHEELID = 1
        self.RIGHTWHEELID = 2
        self.BACKWHEELID = 3

        self.maxspeed = 255  # Absoulute hard limit
        self.speed = 0
        self.direction = 0

        self.currentSpeeds = [0, 0, 0]

    def __exit__(self, exc_type, exc_value, traceback):
        self.SER.close()

    def setRotationDirection(self, motorID, direction):  # driection can be 0 or 1
        id = motorID
        # self.SER.read()
        self.SER.write(str(id) + ":dr" + direction + "\n")
        self.polaarsustest()

    def polaarsustest(self):
        for ID in range(1, 4):
            self.SER.write(str(ID) + ":st" + "\n")

    def setRotationAroundBall(self, speed=20):
        self.currentSpeeds[2] += int(-speed)
        # optional (for smaller turning radius):
        # self.currentSpeeds[0] += sign(speed)*1
        # self.currentSpeeds[1] += sign(speed)*1

    def setRotation(self, speed):
        speed = int(-speed)  # Tee nii, et positiivne suund oleks p�rip�eva p��ramine.

        self.currentSpeeds = map(add, self.currentSpeeds, [speed, speed, speed])

        # return ([speed, speed, speed])

        # for id in range(1, 4):
        #     self.SER.write(str(id) + ":sd" + str(speed) + "\n")

    def setMove(self, direction=0, maxspeed=100):
        x1 = sin(radians(direction - 120))
        x2 = sin(radians(direction + 120))
        x3 = sin(radians(direction))

        x1 = int(x1 * maxspeed)
        x2 = int(x2 * maxspeed)
        x3 = int(x3 * maxspeed)

        self.currentSpeeds = map(add, self.currentSpeeds, [x1, x2, x3])

    def setMoveForward(self, speed=30):
        self.setMove(0, speed)

    def writeToMotors(self):  # x1,x2,x3 are motor speeds
        speeds = self.currentSpeeds
        m = max(speeds)
        if m > 255:
            for i in range(len(speeds)):
                speeds[i] = 255 * (float(speeds[i]) / m)  # adjust all speeds if one of the speeds is > 255

        # self.SER.read()
        self.SER.write(str(LEFTWHEELID) + ":sd" + str(int(round(speeds[0]))) + "\n")
        self.SER.write(str(RIGHTWHEELID) + ":sd" + str(int(round(speeds[1]))) + "\n")
        self.SER.write(str(BACKWHEELID) + ":sd" + str(int(round(speeds[2]))) + "\n")

        logging.debug(self.currentSpeeds)
        self.currentSpeeds = [0, 0, 0]  # reset speeds before next frame (important!)

    #### for reasons, main boardi kontrollimine siin ###
    def isBallSensorBlocked(self):
        logging.debug("getting ball sensor")
        self.SER.timeout = 0.1
        try:
            self.SER.write(self.MAINBOARDID + ':bl\n')
            if time.time() - self.lastKicked < 0.2:
                time.sleep(0.1)
            response = self.SER.readline()
            logging.debug("readline: " + response)
            regex = "<" + self.MAINBOARDID + ":bl:(.*)>\n"
            match = re.match(regex, response)
            self.SER.timeout = None
            return match.group(1) == "1"
        except:
            self.SER.timeout = None
            logging.debug("ballsensorerror")
            return False
    def setDribblerSpeed(self, speed):
        if not isinstance(speed, (int, long)) or speed < 0 or speed > 255:
            logging.debug("ERROR: invalid dribbler speed")
            return
        # self.SER.read()
        self.SER.write(self.MAINBOARDID + ':dm' + str(speed) + '\n')

    def ping(self):
        # self.SER.read()
        self.SER.write(self.MAINBOARDID + ':p\n')

    def charge(self):
        # self.SER.read()
        self.SER.write(self.MAINBOARDID + ':c\n')

    def kick(self):
        self.lastKicked = time.time()
        # self.SER.read()
        self.SER.write(self.MAINBOARDID + ':k\n')
        # self.setMoveForward(100)
        # self.writeToMotors()
        # time.sleep(0.5)


def testrotation(speed=50):  # robot peaks p��rlema vastup�eva
    ser = serial.Serial(PORT, BAUDRATE)
    for id in range(1, 4):
        # logging.debug(id)
        ser.write(str(id) + ":sd" + str(speed) + "\n")
    time.sleep(1)
    # after 1 second stop all motors
    for id in range(1, 4):
        # logging.debug(id)
        ser.write(str(id) + ":sd" + str(0) + "\n")

    ser.close()
    return


def getPID(motorID):
    ID = motorID
    self.SER.write(str(ID) + ":gg" + "\n")
    line = ser.readline()
    return line


def setPID(motorID, P=6, I=8):
    ID = motorID

    # muudab P osa kordajat:
    ser.write(str(ID) + ":pg" + str(P) + "\n")

    # muudab I osa jagajat:
    ser.write(str(ID) + ":ig" + str(I) + "\n")

    ser.write(str(ID) + ":gg" + "\n")
    line = ser.readline()
    logging.debug("new values: " + line)
