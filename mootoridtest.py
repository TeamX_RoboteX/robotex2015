# -*- coding: cp1257 -*-

#Goal:
#Robot can communicate with motor controllers via self written code.

import serial
import time
from math import *
PORT='/dev/ttyACM0'
BAUDRATE=115200

def setRotationDirection(motorID,direction):#driection can be 0 or 1
    ID=motorID
    ser = serial.Serial(PORT, BAUDRATE)
    
    ser.write(str(ID)+":dr"+direction)
    
    ser.close()
    return

def setPID(motorID,P=6,I=8):
    ID=motorID
    ser = serial.Serial(PORT, BAUDRATE)
    
    ser.write(str(ID)+":gg")
    line=ser.readline()
    print("current values: "+line)

    #muudab P osa kordajat:
    ser.write(str(ID)+":pg"+str(P))

    #muudab I osa jagajat:
    ser.write(str(ID)+":ig"+str(I))

    ser.write(str(ID)+":gg")
    line=ser.readline()
    print("new values: "+line)
    
    ser.close()
    return

def polaarsustest(ID):
    ser = serial.Serial(PORT, BAUDRATE)
    
    ser.write(str(ID)+":st")
    
    ser.close()
    return
def renameMotor():
    ser = serial.Serial(PORT, BAUDRATE)
    ser.write("?\n")
    line=ser.readline() #expecting: line = "id:255\n" 
    currentID=line.split(":")[-1]
    currentID=int(currentID)
    print("This motor's ID is currently "+ str(currentID))
    newID=input("Change ID to: ")
    if (type(newID) is int and newID>0):
        ser.write(str(currentID)+":id"+str(newID))
    else:
        print("ERROR! invalid motor ID assigned")
    ser.close()
    return
##renameMotor()


### Liikumine:
# Oletades, et rataste positiivsete liikumissuundade korral roobt pöörleb vastupäeva, 
# 
#võiks töötada järgnev funktsioon:

LEFTWHEELID=1
RIGHTWHEELID=2
BACKWHEELID=3

def testrotation(speed=50):#robot peaks pöörlema vastupäeva
    ser = serial.Serial(PORT, BAUDRATE)
    for id in range(1,4):
        #print(id)
        ser.write(str(id)+":sd"+str(speed))
    time.sleep(1)
    # after 1 second stop all motors
    for id in range(1,4):
        #print(id)
        ser.write(str(id)+":sd"+str(0))
    
    ser.close()
    return


def testwheel(id=255, speed=50):
    ser = serial.Serial(PORT, BAUDRATE)
    ser.write(str(id)+":sd"+str(speed))
    time.sleep(1)
    # after 1 second stop all motors
    ser.write(str(id)+":sd"+str(0))
    ser.close()
    return

def move(direction=0,time=1,maxspeed=100):
    #unsure if works as i want it to, needs testing
    #atm distants täpselt välja arvutamata, praegu lihtsalt liigu x sekundit z suunas y kiirusega.

    #kiirusele 100 vastab pöörlemiskiirus 5,2 pööret/sekundis 
    #70mm dimeetriga ratta joonkiiruseks sellisel pöörlemiskiirusel on ligikaudu 1,14 m/s
    #seega atm peaks ühes sekundis robot läbima umbes meetri (mistahes suunas).
    ser = serial.Serial(PORT, BAUDRATE) 
    
    x1=sin(radians(direction-60))
    x2=sin(radians(direction+60))
    x3=sin(radians(direction))

    ser.write(str(LEFTWHEELID)+":sd"+str(int(x1*maxspeed)))
    ser.write(str(RIGHTWHEELID)+":sd"+str(int(x2*maxspeed)))
    ser.write(str(BACKWHEELID)+":sd"+str(int(x3*maxspeed)))

    time.sleep(time)

    ser.write(str(LEFTWHEELID)+":sd"+str(0))
    ser.write(str(RIGHTWHEELID)+":sd"+str(0))
    ser.write(str(BACKWHEELID)+":sd"+str(0))

    ser.close()
    return


