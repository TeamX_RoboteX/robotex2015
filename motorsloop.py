#!/usr/bin/python
## -*- coding: utf-8 -*-
from lib import vision
from multiprocessing import Process
from time import sleep
import ctypes

from mootorid import *

class obj:
    x = -1;
    y = -1;
    def __init__(self, x, y):
        self.x = x
        self.y = y      

vision.init()

seek=0
rotate=1
move=2
centerwidth=40
rotationspeed=10

# -4 -75

def chooseball(balls):
    closestBall=balls[0]
    for i in range(len(balls)):
        if closestBall.y > balls[i].y:
            closestBall = balls[i]
    return closestBall

def main():
    mootorid = Mootorid()
    action = seek
    while 1:

        ## Get the ball and goal positions:
        objects = vision.getObjects()
        if len(objects) < 4: continue
        blueGate = obj(objects[0], objects[1])
        yellowGate = obj(objects[2], objects[3])
        balls = []
        for i in range(4, len(objects), 2):
            balls.append(obj(objects[i], objects[i+1]))

        closest = False
        #choose closest ball:
        if len(balls)>0:
            closest = chooseball(balls)
        if closest == False:
            mootorid.setRotation(rotationspeed)
            continue
        if abs(closest.x) < centerwidth:
            action = move
        else: action = rotate
        #turn until centered on ball:
        if action == rotate:
            if closest.y > 0:
                direction = 1
            else: direction = -1

            mootorid.setRotation(rotationspeed*direction)
        #move towards ball
        if action == move:
            mootorid.moveForward(30)


if __name__=='__main__':
	main()









