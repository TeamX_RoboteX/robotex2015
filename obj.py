
class obj:
    x = -1;
    y = -1;

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if isinstance(other, obj):
            return self.__dict__ == other.__dict__
        else:
            return False

