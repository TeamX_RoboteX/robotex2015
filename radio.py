
import time, os, serial
import audio
import mootoridtest

def read():
    while 1:
        msg = raw_input()
        if msg.lower().strip() == "win":
            running = open("running.lock", "w")
            running.write("False")
            running.close()
            audio.play("winsong.ogg")
            """
            for i in range(5):
                mootoridtest.testrotation(75)
                time.sleep(1)
                """
        else:
            if len(msg.strip()):
                running = open("running.lock", "w")
                running.write("True")
                running.close()
            else:
                running = open("running.lock", "w")
                running.write("False")
                running.close()
    


def listen(field, id):
#    serial.reset_input_buffer()
    try:
        ser = serial.Serial("/dev/ttyACM0", 9600)
        myField = field
        myID = id
        listening = False
        respond = False
        i = 0
        
        while 1:
            i += 1
            print("waiting for character")
            c = ser.read().decode(errors='replace')
            print("read character"+c)
            if c == "a":
                i = 0
                listening = True
                continue
            if not listening:
                continue # wait for next a
            if i == 1 and c != myField:
                listening = False
            if i == 2 and c != myID and c != "X":
                listening = False
            if i == 2 and c == myID:
                respond = True;
            if i == 2 and c == "X":
                respond = False
            if i == 2 and listening == True:
                msg = ""
                for i in range (3, 12):
                    c = ser.read().decode(errors='replace')
                    if c == "a":
                        i = 0
                        break
                    if c == "-":
                        listening = False
                        break
                    msg += c
                    if msg in ["START", "STOP"]:
                        print(msg)
                        if msg == "STOP":
                            running = open("running.lock", "w")
                            running.write("False")
                            running.close()
                            if respond == True:
                                ser.write(("a"+myField+myID+"ACK-----").encode(errors='replace'))
                            #ser.close()
                            #os._exit(0)
                        elif msg == "START":
                            print("start")
                            running = open("running.lock", "w")
                            running.write("True")
                            running.close()
                            if respond == True:
                                ser.write(("a"+myField+myID+"ACK-----").encode(errors='replace'))
                            #ser.close()
                            #return
    except:
        pass

