# sample signal:
# aBDSTART----

# Currently expects the signal to come in one piece and logs all signals
# and reads a maximum of 36 bytes at once.

import time, serial, logging

logging.basicConfig(filename='radiolog.txt', level=logging.DEBUG, format='%(asctime)s - %(message)s')

def listen(field, id):
    logging.debug("started listening")
    ser = serial.Serial("/dev/ttyACM0", 9600)

    while True:

        msg = ser.read(size=36).decode(errors='replace') # ser.read(ser.inWaiting()) # + raw_input() #

        logging.debug("RECEIVED: "+msg)

        if containssignal('a%(field)s%(id)sSTART----',msg) or containssignal('a%(field)XSTART----',msg):
            print("got message to start " + msg)
            ser.write(("a"+field+id+"ACK-----").encode(errors='replace'))
            # sends "acknowledged" message even if sent to all which  is not illegal, might disrupt other robots.
            # tsitaat reeglitest "Koikidele robotitele saadetud kaskudele vastama ei pea." (kuid j2relikult voib)
            writeTrue()
        elif containssignal('a%(field)s%(id)sSTOP-----',msg) or containssignal('a%(field)XSTOP-----',msg):
            print("got message to stop " + msg)
            ser.write(("a"+field+id+"ACK-----").encode(errors='replace'))
            writeFalse()

def containssignal(string,signal):
    return signal in string

def writeFalse():
    running = open("running.lock", "w")
    running.write("False")
    running.close()
def writeTrue():
    running = open("running.lock", "w")
    running.write("True")
    running.close()

if name() == '__main__':
    # ifmain, test with aBDSTART----
    listen("B", "D")
    writeFalse()