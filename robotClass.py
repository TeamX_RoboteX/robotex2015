# -*- coding: utf-8 -*-

import time, datetime
from operator import add
from obj import obj
import math
import logging
import audio


# logging.basicConfig(filename='./logs/'+str(datetime.datetime.now()), level=logging.DEBUG, format='%(asctime)s - %(message)s')


### Constants ###
RIGHT = 1
LEFT = -1

NONE = 0
SEEK = 1
ROTATE = 2
MOVE = 3
AIM = 4

BLUE = 1
YELLOW = 2

### /Constants ###

### Configuration ###

MOVERANDTIME = 1.2
MOVERANDSPEED = 125 
TOMOVERAND = 5

centerwidth = 100  # currently determines when to start moving towards ball

lookForBallSpeed = 15
slowlyLookForBallSpeed = 7
lookForGoalSpeed = 40
def getAimAtGoalSpeed(goal):
    
    return (abs(goal.x) / 3.5 + 4)/((abs(goal.y)+1.0)/50.0)
def getTurnUntilBallSpeed(ball):
    if ball == False: return 0
    return int(abs(ball.x) / 3 + 0)
def getTurnUntilBallSpeedMoving(ball):
    if ball == False: return 0
    if abs(ball.x) > 37: return 15
    return int(abs(ball.x) / 2.5 * -1 * (ball.y - 100) / 100)
def getMoveToBallSpeed(ball):
    if ball == False: return 25
   
    return ( (ball.y+100) / 2 + 5 ) * 0.8 * ( 100  - abs(ball.x)) / 100.0

### /Configuration ###


class Robot:
    def __init__(self, visiont, mtt, goal):
        global vision, mt, OPPONENTGOALCOLOR
        OPPONENTGOALCOLOR = goal
        vision = visiont
        mt = mtt

        self.surroundings = Surroundings()
        self.noBallsTimer = False
        self.randomMovementTimer = False

        # number of consecutive frames when input signal was not appropriate for current action.
        # only used in loop.py, should it be there?
        # alternatively loopStep() action should be here
        self.wrongSignalCount = 0

    def writeToMotors(self):
        mt.writeToMotors()

    def getNewVisionData(self):
        self.surroundings.update()

    def rotate(self, rotationSpeed):
        mt.setRotation(rotationSpeed)

    def setRotation(self, rotationSpeed):
        mt.setRotation(rotationSpeed)

    def setMoveForward(self, speed):
        mt.setMoveForward(speed)

    def canSeeBall(self):
        if self.surroundings.ballOnScreen():
            return True
        else:
            return False

    def ballInCenter(self):
        if not self.surroundings.ballOnScreen():
            return False
        ball = self.surroundings.closestBall
        if ball == False:
            return False
        if abs(ball.x) < centerwidth:
            return True
        else:
            return False

    def getState(self):
        if self.canSeeBall() or self.surroundings.holdingBall():
            self.noBallsTimer = False
            self.randomMovementTimer = False

        if not self.canSeeBall() and not self.surroundings.holdingBall():
            # 0: Can not see nor sense any ball: look for ball
            return 0
        elif not self.ballInCenter() and not self.surroundings.holdingBall():
            # 1: Ball on screen and not in dribbler (needs to be moved to), but not in center: turn until ball
            # Todo: muuda closestball-i leidmise koodi nii, et kaks samal kaugusel palli ei tekita inf.loopi.
            return 1
        elif not self.surroundings.holdingBall():
            # print("x: "+str(self.surroundings.closestBall.x)+", y: "+str(self.surroundings.closestBall.y))
            # 2: Ball on screen and not in dribbler in center: move to ball
            return 2
        elif not self.surroundings.goalInCenter():
            # 3: ball in dribbler but goal not in center: turn until goal
            return 3
        else:  # self.surroundings.goalInCenter() == True
            # 4: if no more blockers for shooting, shoot
            return 4
            # Optional: add mock code for aiming at goal without sensor input.


    # Actions:
    def lookForBall(self):
        # 0: Can not see nor sense any ball: look for ball
        
        if not self.noBallsTimer:
            self.noBallsTimer = time.time()
        elif not self.randomMovementTimer:
            if time.time() - self.noBallsTimer > TOMOVERAND and self.surroundings.furthestGoalOnScreen():
                # print("new code, moving randomly at speed "+str(MOVERANDSPEED))
                self.randomMovementTimer = time.time()
                self.setMoveForward(MOVERANDSPEED)
                return #perform random movement and stop turning for TOMOVERAND secs.
        else: #random movement is True
            if time.time() - self.randomMovementTimer > MOVERANDTIME:
                self.noBallsTimer = False
                self.randomMovementTimer = False
            else:
                self.setMoveForward(MOVERANDSPEED)
                return #perform random movement and stop turning for 0.7 secs.

        if self.surroundings.ballLastSeen:
            direction = self.surroundings.ballLastSeen
        else:
            direction = 1  # default to turning right
        self.setRotation(lookForBallSpeed * direction)
        # TODO: variable speed

    def slowlyLookForBall(self):
        # ?: Can not see nor sense any ball for long time: look for ball
        if self.surroundings.ballLastSeen:
            direction = self.surroundings.ballLastSeen
        else:
            direction = 1  # default to turning right
        self.setRotation(slowlyLookForBallSpeed * direction)
        # TODO: variable speed
        
    def turnUntilBall(self):
        # 1: Ball on screen and not in dribbler (needs to be moved to), but not in center: turn until ball
        closest = self.surroundings.closestBall
        if closest == False:

            return
        if closest.x > 0:
            direction = 1  # RIGHT
        else:
            direction = -1  # LEFT
        speed = getTurnUntilBallSpeed(closest)
        self.setRotation(speed*direction)
        
    def turnUntilBallMoving(self):
        # 1: Ball on screen and not in dribbler (needs to be moved to), but not in center: turn until ball
        closest = self.surroundings.closestBall
        if closest == False:

            return
        if closest.x > 0:
            direction = 1  # RIGHT
        else:
            direction = -1  # LEFT
        speed = getTurnUntilBallSpeedMoving(closest)
        # print(speed*direction)
        self.setRotation(speed*direction)

    def moveToBall(self):
        # 2: Ball on screen and not in dribbler in center: move to ball
        speed = getMoveToBallSpeed(self.surroundings.closestBall)
        # print("getMoveToBallSpeed(self.surroundings.closestBall):" + str(speed))
        self.setMoveForward(speed)
        self.turnUntilBallMoving()  # Turn while moving foward.

    def aimAtGoal(self):
        # 3: ball in dribbler but goal not in center: turn until goal
        direction = RIGHT
        rotationspeed = lookForGoalSpeed
        if self.surroundings.OppGoalLastSeen != False:
            direction = self.surroundings.OppGoalLastSeen
        if self.surroundings.OppGoal != False:
            rotationspeed = getAimAtGoalSpeed(self.surroundings.OppGoal)
        # self.rotate(rotationspeed * direction)    # dribbler version
        mt.setRotationAroundBall(rotationspeed * direction)  # no dribbler version
        
    def shoot(self):
        audio.shootMusic()
        #self.setMoveForward(6)
        #mt.writeToMotors()
        #time.sleep(1)
        mt.kick()
        time.sleep(0.1)
        self.setRotation(-40)
        self.writeToMotors()
        time.sleep(0.8)

        #self.setMoveForward(255)  # temporary mock code.
        #mt.setDribblerSpeed(0)
        #mt.writeToMotors()
        #time.sleep(1)

    #        self.setRotation(30)

    # Todo: find a way to ignore the ball you just shot towards the goal

    # lisa kuskile ka check, et värav liiga lähedal ei oleks, et ta väravasse ennast kinni ei sõidaks

    def __exit__(self, exc_type, exc_value, traceback):
        pass


class Surroundings:
    def __init__(self):
        self.Balls = []
        self.OppGoal = False
        self.Owngoal = False
        self.update()
        self.closestBall = False
        self.OppGoalLastSeen = False
        self.ballLastSeen = False
        self.closestBallDistance = False  # distance from point (0,-100) (center of dribbler)
        self.furthestGoal = False
        self.lastBlueGoalDist = -200
        self.lastYellowGoalDist = -200
        self.yellowGoal = False
        self.blueGoal = False

    def furthestGoalOnScreen(self):
        furthest=self.furthestGoal
        if furthest == False:
            return False
        if furthest == BLUE:
            return (self.blueGoal != False)
        if furthest == YELLOW:
            return (self.yellowGoal != False)

    def chooseBall(self):
        if len(self.Balls) == 0:
            self.closestBall = False
            return
        balls = self.Balls
        minDistance = 10000000
        closestBall = False
        for ball in balls:
            x, y = ball.x, ball.y + 100
            f = x ** 2 + y ** 2
            if f < minDistance:
                minDistance = f
                closestBall = ball
        self.closestBall = closestBall
        self.closestBallDistance = math.sqrt(minDistance)

    def goalInCenter(self):
        if self.OppGoal == False:
            return False
        if abs(self.OppGoal.x) < 16:

            return True
        else:
            return False


    def holdingBall(self):
        return mt.isBallSensorBlocked()
        closest = self.closestBall
        if closest == False:
            return False
        if self.closestBallDistance < 20:

            return True

    def update(self):
        self.objects = vision.getObjects()
        self.updateBalls()
        self.updateGoals()
        self.chooseBall()
        self.updateBallLastSeen()

    def ballOnScreen(self):
        if self.Balls:
            return True
        else:
            return False

    def updateGoals(self):
        if len(self.objects) < 4: return
        blueGate = obj(self.objects[0], self.objects[1])
        yellowGate = obj(self.objects[2], self.objects[3])

        NoGate = obj(-101, -101)

        if blueGate.y < 0:
            blueGate = NoGate
        if yellowGate.y < 0:
            yellowGate = NoGate

        if blueGate == NoGate:
            blueGate = False
        if yellowGate == NoGate:
            yellowGate = False

        self.yellowGoal = yellowGate
        self.blueGoal = blueGate

        if yellowGate != False:
            self.lastYellowGoalDist = yellowGate.y
        if blueGate != False:
            self.lastBlueGoalDist = blueGate.y

        # Can not see goal is marked by goal==False
        if OPPONENTGOALCOLOR == BLUE:
            self.OppGoal = blueGate
            self.OwnGoal = yellowGate
        if OPPONENTGOALCOLOR == YELLOW:
            self.OppGoal = yellowGate
            self.OwnGoal = blueGate
        if self.OppGoal != False:
            if self.OppGoal.x > 0:
                self.OppGoalLastSeen = RIGHT
            else:
                self.OppGoalLastSeen = LEFT

        if self.lastBlueGoalDist > self.lastYellowGoalDist:
            self.furthestGoal = BLUE
        else:
            self.furthestGoal = YELLOW

    def updateBalls(self):
        self.Balls = []
        if len(self.objects) < 4: return  # error of some sorts (vision not inited)
        for i in range(4, len(self.objects), 2):
            self.Balls.append(obj(self.objects[i], self.objects[i + 1]))

    def updateBallLastSeen(self):
        if self.closestBall:
            if self.closestBall.x > 0:
                self.ballLastSeen = RIGHT
            else:
                self.ballLastSeen = LEFT
