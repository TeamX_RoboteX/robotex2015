

import logging
import datetime


logging.basicConfig(filename='zlogs.txt'+str(datetime.datetime.now()), level=logging.DEBUG, format='%(asctime)s - %(message)s')
