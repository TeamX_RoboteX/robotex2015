import mootorid
import time

def otse():
	mt = mootorid.Mootorid()
	otseaeg = 1.2
	otsekiirus = 125
	algus = time.time()
	print("liigub otse")
	while time.time() - algus < otseaeg:
		mt.setMoveForward(otsekiirus)
		mt.writeToMotors()
	print("Jaab seisma")
	mt.setMoveForward(0)
	mt.writeToMotors()

otse()