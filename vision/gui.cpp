#include <vector>
#include <chrono>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <iostream>
#include "gui.hpp"
#include <map>

using namespace std::chrono;
using namespace std;
using namespace cv;

map<string, long> profilerSums;
map<string, long> profilerCounter;

bool* debugPtr;
bool calibrateable = false;
bool guiElemsAdded = false;
int* imgPtr;

uchar (*orangeCPtr)[3][256];
uchar (*blueCPtr)[3][256];
uchar (*yellowCPtr)[3][256];
uchar (*editable)[3][256] = orangeCPtr;

Mat* framePtr;
Mat* orangePtr;
Mat* bluePtr;
Mat* yellowPtr;
Mat* todisplay = new Mat();

microseconds ms = duration_cast<microseconds>(system_clock::now().time_since_epoch());
microseconds ms2;
long mssum = 0;
long mscount = 0;


void readFromFile() {
  // relative path
  ifstream fin("colors.data");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 256; j++) {
      fin >> (*orangeCPtr)[i][j];
      fin >>  (*blueCPtr)[i][j];
      fin >> (*yellowCPtr)[i][j];
    }
  }
}

void saveToFile() {
  // relative path
  ofstream fout("colors.data");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 256; j++) {
      fout << (*orangeCPtr)[i][j] << " ";
      fout << (*blueCPtr)[i][j] << " ";
      fout << (*yellowCPtr)[i][j] << " ";
    }
  }
}

void add(int event, int x, int y, int flags, void* userdata) {
  if (!calibrateable) return;
  if (event == EVENT_LBUTTONDOWN) {
    Mat hsv;
    cvtColor(*framePtr, hsv, CV_BGR2HSV);
    for (int i = 0; i < 256; i++) {
      if (hsv.at<Vec3b>(Point(x, y)).val[0] - i < 5 && hsv.at<Vec3b>(Point(x, y)).val[0] - i > -5) (*editable)[0][i] = 255;
      if (hsv.at<Vec3b>(Point(x, y)).val[1] - i < 30 && hsv.at<Vec3b>(Point(x, y)).val[1] - i > -30) (*editable)[1][i] = 255;
      if (hsv.at<Vec3b>(Point(x, y)).val[2] - i < 30 && hsv.at<Vec3b>(Point(x, y)).val[2] - i > -30) (*editable)[2][i] = 255;
    }
  }
  saveToFile();
}

void showColor(int, void*) {
  todisplay = framePtr;
}

void showOrange(int, void*) {
  todisplay = orangePtr;
}
void showBlue(int, void*) {
  todisplay = bluePtr;
}
void showYellow(int, void*) {
  todisplay = yellowPtr;
}

void editOrange(int, void*) {
  editable = orangeCPtr;
}

void editBlue(int, void*) {
  editable = blueCPtr;
}

void editYellow(int, void*) {
  editable = yellowCPtr;
}

void resetColor(uchar table[3][256]) {
  for (int i = 0; i < 256; i++) {
    table[0][i] = 0;
    table[1][i] = 0;
    table[2][i] = 0;
  }
}

void resetOrange(int, void*) {
  if (!calibrateable) return;
  resetColor(*orangeCPtr);
}

void resetBlue(int, void*) {
  if (!calibrateable) return;
  resetColor(*blueCPtr);
}

void resetYellow(int, void*) {
  if (!calibrateable) return;
  resetColor(*yellowCPtr);
}

void changeCalibrateable(int data, void*) {
  calibrateable = data;
}

void changeDebug(int data, void*) {
  *debugPtr = data;
}

void addGuiElems() {
  cvCreateButton("color", showColor, NULL, CV_RADIOBOX, 1);
  cvCreateButton("orange", showOrange, NULL, CV_RADIOBOX, 0);
  cvCreateButton("blue", showBlue, NULL, CV_RADIOBOX, 0);
  cvCreateButton("yellow", showYellow, NULL, CV_RADIOBOX, 0);
  createButton("calibrateable",changeCalibrateable,NULL,CV_CHECKBOX,0);
  createButton("debug",changeDebug,NULL,CV_CHECKBOX,*debugPtr);
  
  cvCreateTrackbar("newline", NULL, imgPtr, 15, NULL);
  
  cvCreateButton("orange", editOrange, NULL, CV_RADIOBOX, 1);
  cvCreateButton("blue", editBlue, NULL, CV_RADIOBOX, 0);
  cvCreateButton("yellow", editYellow, NULL, CV_RADIOBOX, 0);
  
  cvCreateButton("orange", resetOrange, NULL);
  cvCreateButton("blue", resetBlue, NULL);
  cvCreateButton("yellow", resetYellow, NULL);
  setMouseCallback("vision", add, NULL);

  guiElemsAdded = true;
}

void profile(string name) {
  if (profilerSums.find(name) == profilerSums.end()) {
    profilerSums[name] = 0;
    profilerCounter[name] = 0;
  }
  ms2 = duration_cast<microseconds>(system_clock::now().time_since_epoch());
  profilerSums[name] += (long)(ms2 - ms).count();
  profilerCounter[name]++;
}

void drawGui(vector<KeyPoint> balls, Point2f blueGate, Point2f yellowGate) {
  if ((*todisplay).empty()) return;
  ms2 = duration_cast<microseconds>(system_clock::now().time_since_epoch());
  mssum +=  1000000 / (long)(ms2 - ms).count();
  mscount++;
  ostringstream strs;
  long last = 0;
  for (const auto& event : profilerSums) {
    strs << event.first << ": " << event.second / profilerCounter[event.first] - last << "; " << endl;
    last = event.second / profilerCounter[event.first];
  }
  strs << mssum / (double)mscount;
  ms = ms2;

  drawKeypoints(*todisplay, balls, *todisplay, Scalar(100,0,100), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
  if (blueGate.x != -1) circle(*todisplay, blueGate, 50, Scalar(255, 0, 0), 3);
  if (yellowGate.x != -1) circle(*todisplay, yellowGate, 50, Scalar(0, 255, 255), 3);
  //drawKeypoints(*todisplay, blueGate, *todisplay, Scalar(255,0,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
  //drawKeypoints(*todisplay, yellowGate, *todisplay, Scalar(0,255,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
  
  line((*todisplay), Point (426,380), Point (426,480), Scalar(0, 0, 0), 3, 8, 0);
  
  imshow("vision", *todisplay);
  displayStatusBar("vision", strs.str());
  if (!guiElemsAdded) addGuiElems();
  waitKey(1);
}

void initGui(Mat* frame, Mat* orange, Mat* blue, Mat* yellow, bool* debug, int* img, uchar (*torangeCPtr)[3][256], uchar (*tblueCPtr)[3][256], uchar (*tyellowCPtr)[3][256]) {
  framePtr = frame;
  orangePtr = orange;
  bluePtr = blue;
  yellowPtr = yellow;
  debugPtr = debug;
  orangeCPtr = torangeCPtr;
  blueCPtr = tblueCPtr;
  yellowCPtr = tyellowCPtr;
  todisplay = framePtr;
  imgPtr = img;
  
  //resetColor(orangeC);
  //resetColor(blueC);
  //resetColor(yellowC);
  readFromFile();


  //  namedWindow("vision");

}
