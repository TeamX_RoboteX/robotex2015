#ifndef GUI_HPP
#define GUI_HPP
#include <vector>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

void profile(string name);
void drawGui(vector<KeyPoint> balls, Point2f blueGate, Point2f yellowGate);
void initGui(Mat* frame, Mat* orange, Mat* blue, Mat* yellow, bool* debug, int* img, uchar (*torangeCPtr)[3][256], uchar (*tblueCPtr)[3][256], uchar (*tyellowCPtr)[3][256]);
#endif /* GUI_HPP */
