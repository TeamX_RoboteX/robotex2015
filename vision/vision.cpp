#include <Python.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <mutex>
#include <thread>
#include <boost/python.hpp>
#include <iostream>
#include <math.h>
#include <string>
#include <sstream>
#include "gui.hpp"

using namespace std;
using namespace cv;
namespace py = boost::python;

// lookup tables
uchar orangeC[3][256];
uchar blueC[3][256];
uchar yellowC[3][256];

unsigned int cb = 1000000000; // count blue blobs
unsigned int cy = 1000000000; // count yellow blobs


vector<int> objects; // to be returned to python
mutex mobjects; // to make multithreading work

bool debug = false;
int img = 0; // debug photo index
int closeness = 5; // multiply by blob size, threshold for another robot stripe closeness
int sameness = 100; // maximum allowed difference in percentages of one colored blob

// resize incoming image
int capW = 854;
int capH = 480;

Mat frame; // from camera
Mat hsv; // after colorspace conversion
// after thresholds:
Mat orange;
Mat blue;
Mat yellow;

//blobs:
vector<KeyPoint> yellows;
vector<KeyPoint> blues;
vector<KeyPoint> balls;

//rects:
vector<Rect> yellowRects;
vector<Rect> blueRects;

//final decided gates
Point2f yellowGate;
Point2f blueGate;

int minGateSize = 70;
int overflowGateSize = 5000;

//lock rect detection
mutex yellowGateMutex;
mutex blueGateMutex;

mutex ballsThread;
mutex yellowsThread;
mutex bluesThread;

VideoCapture capture;

Mat threshold(Mat& I, uchar table[][256]) {
  // accept only char type matrices
  CV_Assert(I.depth() != sizeof(uchar));
  int nRows = I.rows;
  int nCols = I.cols;
  Mat ret(nRows, nCols, CV_8U);
  if (I.isContinuous()) {
    nCols *= nRows;
    nRows = 1;
  }
  int i,j;
  uchar* p;
  uchar* r;
  for( i = 0; i < nRows; i++) {
    p = I.ptr<uchar>(i);
    r = ret.ptr<uchar>(i);
    for ( j = 0; j < nCols; j++) {
      r[j] = table[0][p[j*3]] & table[1][p[j*3 + 1]] & table[2][p[j*3 + 2]];
    }
  }
  Mat element = getStructuringElement(MORPH_RECT, Size(4, 4), Point(2, 2));
  erode(ret, ret, element);
  dilate(ret, ret, element);
  return ret;
}

void findBalls() {
  orange = threshold(hsv, orangeC);

  SimpleBlobDetector::Params params;
  params.filterByArea = true;
  params.minArea = 35;
  params.maxArea = 1000000;
  params.filterByColor = true;
  params.blobColor = 255;
  params.filterByCircularity = false;
  params.filterByConvexity = false;
  params.filterByInertia = false;
  
  Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);

  detector->detect(orange, balls);
  ballsThread.unlock();
}


void findYellowGates() {
  yellow = threshold(hsv, yellowC);
  // Rect detector

  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;

  findContours(yellow, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

  // init empty vectors needed:
  vector<vector<Point> > contours_poly( contours.size() );
  vector<Point2f>center( contours.size() );
  vector<float>radius( contours.size() );
  
  yellowGateMutex.lock();
  yellowRects.clear();

  // Approximate contours to polygons + get bounding rects and circles
  for(unsigned int i = 0; i < contours.size(); i++ ) {
    approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
    yellowRects.push_back(boundingRect(Mat(contours_poly[i])));
    minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
  }
  yellowGateMutex.unlock();

  /// Draw polygonal contour + bonding rects + circles
  for(unsigned int i = 0; i< contours.size(); i++ ) {
    if (yellowRects[i].area() < minGateSize) {
      continue;
    }

    Scalar color = Scalar(255, 255, 255);
    rectangle(yellow, yellowRects[i].tl(), yellowRects[i].br(), color, 2, 8, 0 );
    circle(yellow, center[i], (int)radius[i], color, 2, 8, 0 );
  }

  // filter out all non-gate rects

  vector<Rect> yellowGates;
  vector<Point2f> yellowGatesCircles;
  for (unsigned int y = 0; y < yellowRects.size(); y++) {
    blueGateMutex.lock();
    bool good = true;
    for (unsigned int b = 0; b < blueRects.size(); b++) {

      if (!(max(blueRects[b].br().x, blueRects[b].tl().x) < min(yellowRects[y].tl().x, blueRects[y].br().x) ||
	    min(blueRects[b].br().x, blueRects[b].tl().x) > max(yellowRects[y].tl().x, yellowRects[y].br().x))) {
	if (blueRects[b].area() - yellowRects[y].area() <= overflowGateSize) {
	  // compare size difference
	  good = false;
	  Scalar color = Scalar(0, 255, 0);
	  rectangle(yellow, yellowRects[y].tl(), yellowRects[y].br(), color, 2, 8, 0 );
	}
      }
      if (yellowRects[y].area() < minGateSize) {
	good = false;
      }
    }
    blueGateMutex.unlock();
    if (good) {
      yellowGates.push_back(yellowRects[y]);
      yellowGatesCircles.push_back(center[y]);
    }
  }

  int largestY = 0;
  cy = 1000000000;
  for (unsigned int y = 0; y < yellowGates.size(); y++) {
    if (yellowGates[y].area() > largestY) {
      cy = y;
      largestY = (int) yellowGates[y].area();
    }
  }
  if (cy != 1000000000) yellowGate = yellowGatesCircles[cy];
  else yellowGate = Point2f(-1, -1);
  
  yellowsThread.unlock();
}

void findBlueGates() {
  blue = threshold(hsv, blueC);

  // Rect detector

  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;

  findContours(blue, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

  // init empty vectors needed:
  vector<vector<Point> > contours_poly( contours.size() );
  vector<Point2f>center( contours.size() );
  vector<float>radius( contours.size() );
  
  blueGateMutex.lock();
  blueRects.clear();

  // Approximate contours to polygons + get bounding rects and circles
  for(unsigned int i = 0; i < contours.size(); i++ ) {
    approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
    blueRects.push_back(boundingRect(Mat(contours_poly[i])));
    minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
  }
  blueGateMutex.unlock();

  /// Draw polygonal contour + bonding rects + circles
  for(unsigned int i = 0; i< contours.size(); i++ ) {
    if (blueRects[i].area() < minGateSize) {
      continue;
    }

    Scalar color = Scalar(255, 255, 255);
    //drawContours(blue, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
    rectangle(blue, blueRects[i].tl(), blueRects[i].br(), color, 2, 8, 0 );
    circle(blue, center[i], (int)radius[i], color, 2, 8, 0 );
  }

  // filter out all non-gate rects

  vector<Rect> blueGates;
  vector<Point2f> blueGatesCircles;
  for (unsigned int b = 0; b < blueRects.size(); b++) {
    yellowGateMutex.lock();
    bool good = true;
    for (unsigned int y = 0; y < yellowRects.size(); y++) {
      if (!(max(yellowRects[y].br().x, yellowRects[y].tl().x) < min(blueRects[b].tl().x, blueRects[b].br().x) ||
	    min(yellowRects[y].br().x, yellowRects[y].tl().x) > max(blueRects[b].tl().x, blueRects[b].br().x))) {
	if (blueRects[b].area() - yellowRects[y].area() <= overflowGateSize) {
	  good = false;
	  Scalar color = Scalar(0, 255, 0);
	  rectangle(blue, blueRects[b].tl(), blueRects[b].br(), color, 2, 8, 0 );
	}
      }
      if (blueRects[b].area() < minGateSize) {
	good = false;
      }
    }
    yellowGateMutex.unlock();
    if (good) {
      blueGates.push_back(blueRects[b]);
      blueGatesCircles.push_back(center[b]);
    }
  }

  int largestB = 0;
  cb = 1000000000;
  for (unsigned int b = 0; b < blueGates.size(); b++) {
    if (blueGates[b].area() > largestB) {
      cb = b;
      largestB = (int) blueGates[b].area();
    }
  }
  if (cb != 1000000000) blueGate = blueGatesCircles[cb];
  else blueGate = Point2f(-1, -1);
  
  bluesThread.unlock();
}

void dealFrame() {
  if (debug) {
    Mat src;
    src = imread("fotograafia/my_photo-" + to_string(img+1) + ".jpg", CV_LOAD_IMAGE_COLOR);
    Size size(capW, capH);
    resize(src,frame,size);
  } else {
    capture >> frame;
    //if(frame.empty()) break;
  }
  profile("aFrame");
  cvtColor(frame, hsv, CV_BGR2HSV);
  profile("bCnvrt");

  ballsThread.lock();
  yellowsThread.lock();
  bluesThread.lock();

  thread baThread(findBalls);
  thread yeThread(findYellowGates);
  thread blThread(findBlueGates);

  baThread.detach();
  yeThread.detach();
  blThread.detach();

  /*
  findBalls();
  findYellowGates();
  findBlueGates();
  */
  ballsThread.lock();
  yellowsThread.lock();
  bluesThread.lock();
  ballsThread.unlock();
  yellowsThread.unlock();
  bluesThread.unlock();
  profile("dBlob");

  mobjects.lock();
  objects.clear();
  if (cb == 1000000000) {
    objects.push_back((int)-101);
    objects.push_back((int)-101);
  } else {
    objects.push_back((int)(((int)blueGate.x - ((double)capW / 2)) / (double)capW * 200));
    objects.push_back((int)(((int)blueGate.y - ((double)capH / 2)) / (double)capH * -200));
  }
  if (cy == 1000000000) {
    objects.push_back((int)-101);
    objects.push_back((int)-101);
  } else {
    objects.push_back((int)(((int)yellowGate.x - ((double)capW / 2)) / (double)capW * 200));
    objects.push_back((int)(((int)yellowGate.y - ((double)capH / 2)) / (double)capH * -200));
  }
  for (unsigned int i = 0; i < balls.size(); i++) {
    objects.push_back((int)(((int)balls[i].pt.x - ((double)capW / 2)) / (double)capW * 200));
    objects.push_back((int)(((int)balls[i].pt.y - ((double)capH / 2)) / (double)capH * -200));
  }
  mobjects.unlock();
  profile("eData");
  // gui
  drawGui(balls, blueGate, yellowGate);
}

void see() {
  waitKey(30);
  while (true) {
    dealFrame();
  }
}

void initVision() {
  if (!debug) {
    cout << "opening" << endl;
    capture.open(0);
    if (!capture.isOpened()) {
      cout << "error opening capture" << endl;
    }
    capture.set(CV_CAP_PROP_FRAME_WIDTH, capW);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT, capH);
  }
  
  initGui(&frame, &orange, &blue, &yellow, &debug, &img, &orangeC, &blueC, &yellowC);
  thread visionThread(see);
  visionThread.detach();
  
}

py::list getObjects() {
  mobjects.lock();
  py::list l;
  for(auto &i : objects) l.append(i);
  mobjects.unlock();
  return l;
}

BOOST_PYTHON_MODULE(vision) {
  using namespace boost::python;
  def("init", initVision);
  def("getObjects", getObjects);
}
