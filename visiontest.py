#!/usr/bin/python
from lib import vision
from multiprocessing import Process
from time import sleep
import ctypes

class obj:
	x = -1;
	y = -1;
	def __init__(self, x, y):
		self.x = x
		self.y = y		

vision.init()
while 1:
	objects = vision.getObjects()
	if len(objects) < 4: continue
	blueGate = obj(objects[0], objects[1])
	yellowGate = obj(objects[2], objects[3])
	balls = []
	for i in range(4, len(objects), 2):
		balls.append(obj(objects[i], objects[i+1]))


	print "blueGate", blueGate.x, blueGate.y
	print "yellowGate", yellowGate.x, yellowGate.y
	for ball in balls:
		print "ball", ball.x, ball.y

	sleep(1)

